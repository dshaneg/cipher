import unittest

from cipher.caesar import Caesar


class TestCaesarCipherEncrypt(unittest.TestCase):
    def test_hello(self):
        caesar = Caesar(3)
        self.assertEqual('KHOOR', caesar.encrypt('hello'))

    def test_default(self):
        caesar = Caesar()
        self.assertEqual('KHOOR', caesar.encrypt('hello'))

    def test_with_punctuation(self):
        caesar = Caesar()
        self.assertEqual('KHOOR HCUD!', caesar.encrypt('hello ezra!'))

    def test_bigoffset(self):
        caesar = Caesar(27)
        self.assertEqual('IFMMP', caesar.encrypt('hello'))


class TestCaesarCipherDecrypt(unittest.TestCase):
    def test_hello(self):
        caesar = Caesar(3)
        self.assertEqual('HELLO', caesar.decrypt('KHOOR'))

    def test_default(self):
        caesar = Caesar()
        self.assertEqual('HELLO', caesar.decrypt('KHOOR'))

    def test_with_punctuation(self):
        caesar = Caesar()
        self.assertEqual('HELLO EZRA!', caesar.decrypt('KHOOR HCUD!'))

    def test_bigoffset(self):
        caesar = Caesar(27)
        self.assertEqual('HELLO', caesar.decrypt('IFMMP'))


if __name__ == '__main__':
    unittest.main()
