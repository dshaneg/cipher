import string
import click
from cipher.caesar import Caesar


@click.group()
def cli():
    pass


@cli.command()
@click.option('-o', '--offset', default=3, help='Number of letters to shift.')
@click.argument('message')
def encrypt(offset, message):
    caesar = Caesar(offset)
    click.echo(caesar.encrypt(message))


@cli.command()
@click.option('-o', '--offset', default=3, help='Number of letters to shift.')
@click.argument('message')
def decrypt(offset, message):
    caesar = Caesar(offset)
    click.echo(caesar.decrypt(message))


@cli.command()
@click.option('-o', '--offset', default=3, help='Number of letters to shift.')
def key(offset):
    click.echo('Plain:  ' + string.ascii_uppercase)
    caesar = Caesar(offset)
    click.secho('Cipher: ' + caesar.encrypt(string.ascii_uppercase), fg='red')


if __name__ == '__main__':
    cli()
